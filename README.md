[![pipeline status](https://gitlab.com/blabuschagne/eslint-plugin-vanilla-i18n/badges/master/pipeline.svg)](https://gitlab.com/blabuschagne/eslint-plugin-vanilla-i18n/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# eslint-plugin-vanilla-i18n

Detect and autofix strings which require externalization in Vanilla JS files.

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-vanilla-i18n`:

```
$ npm install eslint-plugin-vanilla-i18n --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-vanilla-i18n` globally.

## Usage

Add `vanilla-i18n` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

JSON:

```json
{
  "plugins": ["vanilla-i18n"]
}
```

YAML:

```yaml
plugins:
  - vanilla-i18n
```

Then configure the rules you want to use under the rules section.

JSON:

```json
{
  "rules": {
    "vanilla-i18n/detect-non-i18n-string": 2
  }
}
```

YAML:

```yaml
rules:
  vanilla-i18n/detect-non-i18n-string: 2
```

## Supported Rules

- [detect-non-i18n-string](https://gitlab.com/blabuschagne/eslint-plugin-vanilla-i18n/blob/master/docs/rules/detect-non-i18n-string.md)
